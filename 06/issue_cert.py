#!/usr/bin/env python3

import argparse, codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# took 5.5 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='issue TLS server certificate based on CSR', add_help=False)
parser.add_argument("private_key_file", help="Private key file (in PEM or DER form)")
parser.add_argument("CA_cert_file", help="CA certificate (in PEM or DER form)")
parser.add_argument("csr_file", help="CSR file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store certificate (in PEM form)")
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

#==== ASN1 encoder start ====
# put your DER encoder functions here

def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_null():
    # returns DER encoding of NULL
    return bytes([0x05]) + bytes([0x00])

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([0x30]) + asn1_len(der) + der

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = b''
    if i == 0:
        integer = bytes([0x00])
    else:
        while i:
            last = bytes([i & 0xFF])
            integer = last + integer
            i = i >> 8

        if last >= b'\x80':
            integer = b'\x00' + integer

    return bytes([0x02]) + asn1_len(integer) + integer

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([0x31]) + asn1_len(der) + der

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 | tag]) + asn1_len(der) + der

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([0x17]) + asn1_len(time) + time

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    
    #Padding
    length = len(bitstr)
    padding = (8 - (length % 8)) % 8
    bitstr = bitstr + '0'*padding


    bitstr_int = 0
    bitstr_bytes = b''
    cont = 0
    for bit in bitstr:
        cont = cont + 1
        bitstr_int = bitstr_int << 1
        if bit == '1':
            bitstr_int = bitstr_int | 1
        #Every 8 bits we get the byte
        if cont == 8:
            #we have the byte
            bitstr_bytes = bitstr_bytes + bytes([bitstr_int])
            bitstr_int = 0
            cont = 0



    return bytes([0x03]) + asn1_len(bytes([padding]) + bitstr_bytes) + bytes([padding]) + bitstr_bytes

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([0x13]) + asn1_len(string) + string

def asn1_bitstring_der(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of BITSTRING
    return bytes([0x03]) + asn1_len( b'\x00' + string ) + b'\x00' + string   

#==== ASN1 encoder end ====

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    content = b''.join(content.splitlines()[1:-1])
    content = codecs.decode(content, 'base64')
    return content

def get_privkey(filename):
    # reads private key file and returns (n, d)
    with open(filename, 'rb') as p_file:
        private_file = p_file.read()
    
    if b'-----BEGIN RSA PRIVATE KEY-----' in private_file:
        privkey = decoder.decode(pem_to_der(private_file))
    else:
        privkey = decoder.decode(private_file)

    return int(privkey[0][1]), int(privkey[0][3])

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5
    # calculate byte length of modulus n
    bytes_number = (n.bit_length() + 7) // 8

    # plaintext must be at least 3 bytes smaller than modulus
    if len(plaintext) > (bytes_number - 3):
        print('[-] Plaintext is too long for this key. Must be smaller than', (bytes_number - 3))
        exit()

    # generate padding bytes
    padding_length = bytes_number - len(plaintext) - 3
    padded_plaintext = b'\x00\x01' + b'\xFF' * padding_length + b'\x00' + plaintext
    return padded_plaintext

def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of m
    der = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([2, 16, 840, 1, 101, 3, 4, 2, 1]) +
            asn1_null()) +
        asn1_octetstring(hashlib.sha256(m).digest()))

    return der

def sign(m, keyfile):
    # sign DigestInfo of message m
    n, d = get_privkey(keyfile)

    der = digestinfo_der(m)
    padded_der = pkcsv15pad_sign(der, n)
    
    mod_length = (n.bit_length() + 7) // 8
    signature = nb(pow(bn(padded_der),d,n), mod_length)

    return signature

def get_subject_cn(csr_der):
    # return CommonName value from CSR's Distinguished Name
    # looping over Distinguished Name entries until CN found
    distinguished_names = decoder.decode(csr_der)[0][0][1]
    for entry in distinguished_names:
        if list(entry[0][0]) == [2, 5, 4, 3]:
            return entry[0][1]

def get_subjectPublicKeyInfo(csr_der):
    # returns DER encoded subjectPublicKeyInfo from CSR
    return encoder.encode(decoder.decode(csr_der)[0][0][2])

def get_subjectName(cert_der):
    # return subject name DER from CA certificate
    return encoder.encode(decoder.decode(cert_der)[0][0][5])


def issue_certificate(private_key_file, issuer, subject, pubkey):
    # receives CA private key filename, DER encoded CA Distinguished Name, constructed DER encoded subject's Distinguished Name, DER encoded subjectPublicKeyInfo
    # returns X.509v3 certificate in PEM format
  
    # TODO change variables ...
    # construct tbsCertificate structure
    tbsCertificate = asn1_sequence(
        # version = [0]
        asn1_tag_explicit(asn1_integer(2), 0) +
        # serialNumber = 666
        asn1_integer(666) +
        # signature = sha256WithRSAEncryption
        asn1_sequence(
            asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) +
            asn1_null()) +
        issuer + 

        #TODO edit time
        # validity (hardcoded from Mar 22 2018, 00:00:00 to Jun 22 2018, 00:00:00)
        asn1_sequence(
            asn1_utctime(b'200320000000Z') +
            asn1_utctime(b'200630000000Z')) +

        # subject CN
        subject +

        # subjectPublicKeyInfo
        pubkey +

        # extensions [3]
        asn1_tag_explicit(
            asn1_sequence(
                # basicConstraints = false
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 19]) +
                    asn1_boolean(True) +
                    asn1_octetstring(asn1_sequence(asn1_boolean(False)))) +

                # keyUsage = true;  digitalSignature (0) = 100000000
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 15]) +
                    asn1_boolean(True) +
                    asn1_octetstring(asn1_bitstring('10000000'))) +

                # extended key usage: id-kp-serverAuth
                asn1_sequence(
                    asn1_objectidentifier([2, 5, 29, 37]) +
                    asn1_boolean(True) +
                    asn1_octetstring(asn1_sequence(asn1_objectidentifier([1, 3, 6, 1, 5, 5, 7, 3, 1]))))), 
            3))

    # sign tbsCertificate structure
    signature = sign(tbsCertificate, private_key_file)
    #int_signature = bn(signature)
    #bitstring_signature = bin(int_signature)[2:]

    # construct final X.509 DER
    der_final = asn1_sequence(
        tbsCertificate +
        asn1_sequence(
            asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) +
            asn1_null()) +
        asn1_bitstring_der(signature))

    # convert to PEM and adding PEM headers
    pem = b'-----BEGIN CERTIFICATE-----\n' + codecs.encode(der_final, 'base64') + b'-----END CERTIFICATE-----\n'

    return pem


# obtain subject's CN from CSR
csr_der = pem_to_der(open(args.csr_file, 'rb').read())
subject_cn_text = get_subject_cn(csr_der)

print("[+] Issuing certificate for \"%s\"" % (subject_cn_text))

# obtain subjectPublicKeyInfo from CSR
pubkey = get_subjectPublicKeyInfo(csr_der)

# construct subject name DN
subject = asn1_sequence(
    asn1_set(asn1_sequence(
        asn1_objectidentifier([2, 5, 4, 3]) +
        asn1_printablestring(str(subject_cn_text).encode()))))

# get subject name DN from CA certificate
CAcert = pem_to_der(open(args.CA_cert_file, 'rb').read())
CAsubject = get_subjectName(CAcert)

# issue certificate
cert_pem = issue_certificate(args.private_key_file, CAsubject, subject, pubkey)
open(args.output_cert_file, 'wb').write(cert_pem)
