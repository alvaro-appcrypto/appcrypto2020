#!/usr/bin/env python3
import os, sys       # do not use any other imports/libraries
# took 2.5 hours (please specify here how much time your solution required)

def bn(b):
    # b - bytes to encode as integer
    i = 0
    for char in b:
        i = i << 8
        i = i | char
    return i

def nb(i, length):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    b = b''
    for byte in range(length):
        b = bytes([(i & 0xFF)]) + b
        i = i >> 8
    return b

def encrypt(pfile, kfile, cfile):
    plaintext = open(pfile, 'rb').read()
    plaintext_int = bn(plaintext)
    
    key = os.urandom(len(plaintext))
    key_int = bn(key)

    ciphertext_int = plaintext_int ^ key_int
    ciphertext = nb(ciphertext_int, len(plaintext))

    ciphertext_file = open(cfile, 'wb')
    ciphertext_file.write(ciphertext)

    key_file = open(kfile, 'wb')
    key_file.write(key)

    pass

def decrypt(cfile, kfile, pfile):
    ciphertext = open(cfile, 'rb').read()
    ciphertext_int = bn(ciphertext)

    key = open(kfile, 'rb').read()
    key_int = bn(key)

    plaintext_int = ciphertext_int ^ key_int
    plaintext = nb(plaintext_int, len(ciphertext))

    plaintext_file = open(pfile, 'wb')
    plaintext_file.write(plaintext)

    pass

def usage():
    print("Usage:")
    print("encrypt <plaintext file> <output key file> <ciphertext output file>")
    print("decrypt <ciphertext file> <key file> <plaintext output file>")
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
