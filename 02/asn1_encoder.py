#!/usr/bin/env python
import sys   # do not use any other imports/libraries

# took 5.5 hours (please specify here how much time your solution required)

#REMOVE?
def nb(i, length):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    b = b''
    for byte in range(length):
        b = bytes([(i & 0xFF)]) + b
        i = i >> 8
    return b

def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return bytes([0x05]) + bytes([0x00])

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = b''
    if i == 0:
        integer = bytes([0x00])
    else:
        while i:
            last = bytes([i & 0xFF])
            integer = last + integer
            i = i >> 8

        if last >= b'\x80':
            integer = b'\x00' + integer

    return bytes([0x02]) + asn1_len(integer) + integer

def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    
    #Padding
    length = len(bitstr)
    padding = (8 - (length % 8)) % 8
    bitstr = bitstr + '0'*padding

    bitstr_int = 0
    bitstr_bytes = b''
    cont = 0
    for bit in bitstr:
        cont = cont + 1
        bitstr_int = bitstr_int << 1
        if bit == '1':
            bitstr_int = bitstr_int | 1
        #Every 8 bits we get the byte
        if cont == 8:
            #we have the byte
            bitstr_bytes = bitstr_bytes + bytes([bitstr_int])
            bitstr_int = 0
            cont = 0

    return bytes([0x03]) + asn1_len(bytes([padding]) + bitstr_bytes) + bytes([padding]) + bitstr_bytes

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j 

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([0x30]) + asn1_len(der) + der

def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([0x31]) + asn1_len(der) + der

def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([0x13]) + asn1_len(string) + string

def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([0x17]) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 | tag]) + asn1_len(der) + der

# figure out what to put in '...' by looking on ASN.1 structure required (see slides)
#asn1 = asn1_tag_explicit(asn1_sequence( asn1_boolean(True) + asn1_bitstring("110") ), 0)
asn1 = asn1_tag_explicit(asn1_sequence(
    asn1_set(  
        asn1_integer(5) +
        asn1_tag_explicit(asn1_integer(200),2) +
        asn1_tag_explicit(asn1_integer(65407),11)
    ) + asn1_boolean(True) + asn1_bitstring("110") 
    + asn1_octetstring(b"\x00\x01"+b"\x02"*49) + asn1_null()
    + asn1_objectidentifier([1,2,840,113549,1])
    + asn1_printablestring(b"hello.") + asn1_utctime(b"150223010900Z") 
    ), 0)
open(sys.argv[1], 'wb').write(asn1)
