#!/usr/bin/python3

import codecs, hashlib, sys
from pyasn1.codec.der import decoder
sys.path = sys.path[1:] # don't remove! otherwise the library import below will try to import your hmac.py
import hmac # do not use any other imports/libraries

# took 4.5 hours (please specify here how much time your solution required)
algorithms = {'1.2.840.113549.2.5': 'md5', '1.3.14.3.2.26': 'sha1', '2.16.840.1.101.3.4.2.1': 'sha256'}


def verify(filename):
    print("[+] Reading HMAC DigestInfo from", filename+".hmac")
    der = open(filename + '.hmac', 'rb').read()
   
    algorithm = algorithms[str(decoder.decode(der)[0][0][0])]
    digest = decoder.decode(der)[0][1]

    print("[+] HMAC-" + algorithm, "digest:", digest._value.hex())

    key = input("[?] Enter key: ").encode()

    print(algorithm)
    digest2 = calc_hmac(filename, key, algorithm)
    print("[+] Calculated HMAC-" + algorithm + ":", digest2.hex())

    if digest2 != digest:
        print("[-] Wrong key or message has been manipulated!")
    else:
        print("[+] HMAC verification successful!")


#Added mac calculation for each algorithm
def mac(filename, algorithm='sha256'):
    key = input("[?] Enter key: ").encode()

    if algorithm == 'sha256':
        digest = calc_hmac(filename, key, 'sha256')
        obj_identifier = [2, 16, 840, 1, 101, 3, 4, 2, 1]
    
    elif algorithm == 'sha1':
        digest = calc_hmac(filename, key, 'sha1')
        obj_identifier = [1, 3, 14, 3, 2, 26]

    elif algorithm == 'md5':
        digest = calc_hmac(filename, key, 'md5')
        obj_identifier = [1, 2, 840, 113549, 2, 5]

    print("[+] Calculated HMAC-" + algorithm + ":", digest.hex())

    digestInfo = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier(obj_identifier) +
            asn1_null()) +
        asn1_octetstring(digest))

    print("[+] Writing HMAC DigestInfo to", filename+".hmac")
    open(filename + '.hmac', 'wb').write(digestInfo)



def calc_hmac(filename, key, algorithm):
    if algorithm == 'md5':
        hash_hmac = hmac.new(key, None, hashlib.md5)
    elif algorithm == 'sha1':
        hash_hmac = hmac.new(key, None, hashlib.sha1)
    elif algorithm == 'sha256':
        hash_hmac = hmac.new(key, None, hashlib.sha256)

    with open(filename, 'rb') as content:
        while True:
            chunk = content.read(512)
            if not chunk:
                break
            hash_hmac.update(chunk)

    return hash_hmac.digest()



#ASN1 definitions
def asn1_len(value_bytes):
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_octetstring(octets):
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j 

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes

def asn1_sequence(der):
    return bytes([0x30]) + asn1_len(der) + der

def asn1_null():
    return bytes([0x05]) + bytes([0x00])

#End ASN1 definition

def usage():
    print("Usage:")
    print("-verify <filename>")
    print("-mac <filename>")
    sys.exit(1)

#Edited to allow mac on every algorithm
if len(sys.argv) < 3:
    print(len(sys.argv))
    usage()
elif sys.argv[1] == '-mac':
    if len(sys.argv) == 4:
        mac(sys.argv[2], sys.argv[3])
    else:
        mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()
