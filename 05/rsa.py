#!/usr/bin/env python3

import codecs, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# took 4.5 hours (please specify here how much time your solution required)


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

# ASN1 functions
def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_null():
    # returns DER encoding of NULL
    return bytes([0x05]) + bytes([0x00])

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([0x30]) + asn1_len(der) + der

# End ASN1 functions


def pem_to_der(content):
    # converts PEM content to DER
    content = b''.join(content.splitlines()[1:-1])
    content = codecs.decode(content, 'base64')
    return content

def get_pubkey(filename):
    # reads public key file and returns (n, e)
    # decode the DER to get public key DER structure, which is encoded as BITSTRING
    
    with open(filename, 'rb') as p_file:
        pubkey_file = p_file.read()

    if b'-----BEGIN PUBLIC KEY-----' in pubkey_file: 
        der_pubkey = decoder.decode(pem_to_der(pubkey_file))
    else:
        der_pubkey = decoder.decode(pubkey_file)
    bitstring = str(der_pubkey[0][1])

    # convert BITSTRING to bytestring
    bytestring = nb( int(str(der_pubkey[0][1]), 2))
    
    # decode the bytestring (which is actually DER) and return (n, e)
    pubkey = decoder.decode(bytestring)[0] 
    
    return int(pubkey[0]), int(pubkey[1])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    with open(filename, 'rb') as p_file:
        private_file = p_file.read()
    
    if b'-----BEGIN RSA PRIVATE KEY-----' in private_file:
        privkey = decoder.decode(pem_to_der(private_file))
    else:
        privkey = decoder.decode(private_file)
    
    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5
    # calculate number of bytes required to represent the modulus n
    bytes_number = (n.bit_length() + 7) // 8

    # plaintext must be at least 11 bytes smaller than the modulus
    if len(plaintext) > bytes_number - 11:
        print('[-] Plaintext is too long for this key. Must be smaller than', (bytes_number - 11))
        exit()

    # generate padding bytes
    padding_length = bytes_number - len(plaintext) - 3
    random_pad = os.urandom(padding_length)
    while b'\x00' in random_pad:
        random_pad = random_pad.replace(b'\x00', os.urandom(1), 1)

    padded_plaintext = b'\x00\x02' + random_pad + b'\x00' + plaintext

    return padded_plaintext

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5
    # calculate byte length of modulus n
    bytes_number = (n.bit_length() + 7) // 8

    # plaintext must be at least 3 bytes smaller than modulus
    if len(plaintext) > bytes_number - 3:
        print('[-] Plaintext is too long for this key. Must be smaller than', (bytes_number - 3))
        exit()

    # generate padding bytes
    padding_length = bytes_number - len(plaintext) - 3
    padded_plaintext = b'\x00\x01' + b'\xFF' * padding_length + b'\x00' + plaintext

    return padded_plaintext

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    plaintext = plaintext[2:].split(b'\x00', 1)[1]
    return plaintext

def encrypt(keyfile, plaintextfile, ciphertextfile):
    n, e = get_pubkey(keyfile)
    
    with open(plaintextfile, 'rb') as plain_file:
        padded_plaintext = bn(pkcsv15pad_encrypt(plain_file.read(), n))

    with open(ciphertextfile, 'wb') as cipher_file:
        ciphertext = nb( pow(padded_plaintext, e, n) )
        cipher_file.write(ciphertext)

def decrypt(keyfile, ciphertextfile, plaintextfile):
    n, d = get_privkey(keyfile)
    
    with open(ciphertextfile, 'rb') as cipher_file:
        ciphertext = cipher_file.read()

    cipher_int = bn(ciphertext)
    padded_plaintext = nb(pow(cipher_int,d,n))
    plaintext = pkcsv15pad_remove(padded_plaintext) 

    with open(plaintextfile, 'wb') as plain_file:
        plain_file.write(plaintext)

def calc_sha256(filename):
    with open(filename, 'rb') as f_file:
        digest = hashlib.sha256()
        while True:
            chunk = f_file.read(512)
            if not chunk:
                break
            digest.update(chunk)

    return digest.digest()

def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of file
    der = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([2, 16, 840, 1, 101, 3, 4, 2, 1]) +
            asn1_null()) +
        asn1_octetstring(calc_sha256(filename)))

    return der

def sign(keyfile, filetosign, signaturefile):
    n, d = get_privkey(keyfile)

    der = digestinfo_der(filetosign)
    padded_der = pkcsv15pad_sign(der, n)
    
    mod_length = (n.bit_length() + 7) // 8
    signature = nb(pow(bn(padded_der),d,n), mod_length)

    with open(signaturefile, 'wb') as sig_file: 
        sig_file.write(signature)
 
    # Warning: make sure that signaturefile produced has the same
    # length as the modulus (hint: use parametrized nb()).

def verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification failure"
    n, e = get_pubkey(keyfile)

    with open(signaturefile, 'rb') as sig_file:
        signature = sig_file.read()

    padded_verify = nb(pow(bn(signature), e, n))
    der_verify = pkcsv15pad_remove(padded_verify)
    hash_verify = decoder.decode(der_verify)[0][1]

    if hash_verify == calc_sha256(filetoverify):
        print("Verified OK")
    else:
        print("Verification failure")

def usage():
    print("Usage:")
    print("encrypt <public key file> <plaintext file> <output ciphertext file>")
    print("decrypt <private key file> <ciphertext file> <output plaintext file>")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)


if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()

