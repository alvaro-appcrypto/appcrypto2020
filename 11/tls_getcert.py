#!/usr/bin/env python3

import argparse, codecs, datetime, os, socket, sys, time # do not use any other imports/libraries
from urllib.parse import urlparse

# took 3.0 hours (please specify here how much time your solution required)

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.2 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

# returns TLS record that contains ClientHello Handshake message
def client_hello():

    print("--> ClientHello()")

    # list of cipher suites the client supports
    csuite = b"\x00\x05" # TLS_RSA_WITH_RC4_128_SHA
    csuite+= b"\x00\x2f" # TLS_RSA_WITH_AES_128_CBC_SHA
    csuite+= b"\x00\x35" # TLS_RSA_WITH_AES_256_CBC_SHA

    # add handshake message header
    handshake_type = b"\x01"  # ClientHello (type 0x01)

    handshake_message = b"\x03\x03"  # TLS version = 1.2 (0x0303)
    handshake_message += nb(int(time.time()), 4) + os.urandom(28)  # 4 bytes timestamp + 28 bytes random
    handshake_message += b"\x20" + os.urandom(32)  # Session ID length + sessid ID
    handshake_message += nb(len(csuite), 2) + csuite  # Cipher suites length + cipher suites list
    handshake_message += b"\x01\x00"  # Compression methods length + null (0x00)

    handshake_layer = handshake_type + nb(len(handshake_message), 3) + handshake_message

    # add record layer header
    record_layer = b"\x16\x03\x03"  # Handsake (0x16) + TLS 1.2 (0x0303)

    record = record_layer + nb(len(handshake_layer), 2) + handshake_layer

    return record

# returns TLS record that contains 'Certificate unknown' fatal Alert message
def alert():
    print("--> Alert()")

    # add alert message
    alert_message = b"\x02\x2e"  # Fatal 0x02

    # add record layer header
    record_layer = b"\x15"  # Alert 0x15

    record = record_layer + nb(len(alert_message), 2) + alert_message

    return record

# parse TLS Handshake messages
def parsehandshake(r):
    global server_hello_done_received

    # read Handshake message type and length from message header
    htype = r[0]
    length = bn(r[1:4])

    if htype == 0x02:
        print("	<--- ServerHello()")

        server_random = r[6:6 + 32]

        gmt = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S')

        sessid_length = r[38]
        sessid = r[39:39 + sessid_length]
        
        print("	[+] server randomness:", server_random.hex().upper())
        print("	[+] server timestamp:", gmt)
        print("	[+] TLS session ID:", sessid.hex().upper())

        cipher = r[39 + sessid_length:41 + sessid_length]

        if cipher == b"\x00\x2f":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA")
        elif cipher == b"\x00\x35":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA")
        elif cipher == b"\x00\x05":
            print("	[+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA")
        else:
            print("[-] Unsupported cipher suite selected:", cipher.hex())
            sys.exit(1)

        compression = r[41+sessid_length]

        if compression != 0x00:
            print("[-] Wrong compression:", compression)
            sys.exit(1)

    elif htype == 0x0b:
        print("	<--- Certificate()")

        certlen = bn(r[7:10])
        print("	[+] Server certificate length:", certlen)
        if args.certificate:
            certificate = r[10:10 + certlen]

            with open(args.certificate, 'wb') as f_certificate:
                f_certificate.write(b"-----BEGIN CERTIFICATE-----\n" + codecs.encode(certificate, 'base64') + b"-----END CERTIFICATE-----")

            print("	[+] Server certificate saved in:", args.certificate)

    elif htype == 0x0e:
        print("<--- ServerHelloDone()")
        server_hello_done_received = True
    else:
        print("[-] Unknown Handshake type:", htype)
        sys.exit(1)

    # handle the case of several handshake messages in one record
    leftover = r[4 + length:]
    if len(leftover):
        parsehandshake(leftover)



# parses TLS record
def parserecord(r):
    # parse TLS record header and pass the record body to the corresponding parsing method (i.e., parsehandshake())
    content_type = r[0]
    length = bn(r[3:5])

    if content_type == 0x16:
        parsehandshake(r[5:5 + length])


# read from the socket full TLS record
def readrecord():
    global s

    record = b""
    # read the TLS record header (5 bytes)
    for i in range(5):
        record += s.recv(1)

    # find data length
    length = bn(record[3:])

    # read the TLS record body
    for i in range(length):
        record += s.recv(1)

    return record


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]
path = url.path

s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
while not server_hello_done_received:
    parserecord(readrecord())
s.send(alert())

print("[+] Closing TCP connection!")
s.close()

