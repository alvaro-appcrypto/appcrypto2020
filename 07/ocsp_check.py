#!/usr/bin/env python3

import codecs, datetime, hashlib, re, sys, socket # do not use any other imports/libraries
from urllib.parse import urlparse
from pyasn1.codec.der import decoder, encoder
from pyasn1.type import namedtype, univ

# sudo apt install python3-pyasn1-modules
from pyasn1_modules import rfc2560, rfc5280

# took 4.0 hours (please specify here how much time your solution required)

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

#==== ASN1 encoder start ====
# put your DER encoder functions here

def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_null():
    # returns DER encoding of NULL
    return bytes([0x05]) + bytes([0x00])

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes

def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([0x30]) + asn1_len(der) + der

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = b''
    if i == 0:
        integer = bytes([0x00])
    else:
        while i:
            last = bytes([i & 0xFF])
            integer = last + integer
            i = i >> 8

        if last >= b'\x80':
            integer = b'\x00' + integer

    return bytes([0x02]) + asn1_len(integer) + integer

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    return bytes([0xa0 | tag]) + asn1_len(der) + der

#==== ASN1 encoder end ====

def pem_to_der(content):
    # converts PEM encoded X.509 certificate (if it is PEM) to DER
    if b'----' in content:
        content = b''.join(content.splitlines()[1:-1])
        content = codecs.decode(content, 'base64')
    return content

def get_name(cert):
    # get subject DN from certificate
    return encoder.encode(decoder.decode(cert)[0][0][5])

def get_key(cert):
    # get subjectPublicKey from certificate

    bitstring = decoder.decode(cert)[0][0][6][1]
    bitstr_int = 0
    cont = 0
    for bit in bitstring:
        cont = cont + 1
        bitstr_int = bitstr_int << 1
        if bit == 1:
            bitstr_int = bitstr_int | 1

    bytes_number = (bitstr_int.bit_length()+7)//8
    bytestring = nb(bitstr_int, bytes_number)

    return bytestring

def get_serial(cert):
    # get serial from certificate
    return decoder.decode(cert)[0][0][1]

def produce_request(cert, issuer_cert):
    # make OCSP request in ASN.1 DER form

    serial = get_serial(cert)
    pub_key = get_key(issuer_cert)
    name = get_name(issuer_cert)

    pub_key_hash = hashlib.sha1(pub_key).digest()
    name_hash = hashlib.sha1(name).digest()

    # construct CertID (use SHA1)
    certID = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([1, 3, 14, 3, 2, 26]) +
            asn1_null()) + 
        asn1_octetstring(name_hash) + 
        asn1_octetstring(pub_key_hash) +
        asn1_integer(serial)
        )

    print("[+] Querying OCSP for serial:", serial)

    # construct entire OCSP request
    request = asn1_sequence(
        asn1_sequence(
        asn1_sequence(
            #asn1_tag_explicit(asn1_integer(0), 0) +
            asn1_sequence(certID)
            )
        )
        )

    # TODO remove
    # maybe I need one more sequence
    with open('oscp_request.der', 'wb') as file:
        file.write(request)

    return request

def send_req(ocsp_req, ocsp_url):
    # send OCSP request to OCSP responder

    # parse ocsp responder url
    url = urlparse(ocsp_url)
    host = url.netloc

    print("[+] Connecting to %s..." % (host))
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((url.netloc, 80))

    content_length = len(ocsp_req)
    # send HTTP POST request
    type_header = "\r\nContent-Type: application/ocsp-request"
    length_header = "\r\nContent-Length: " + str(content_length)
    headers = "POST " + url.path + " HTTP/1.1\r\nHost: " + host + type_header + length_header + "\r\n\r\n"
    request = headers.encode() + ocsp_req
    s.send(request)

    # read HTTP response header
    headers = b''
    while b"\r\n\r\n" not in headers:
        received = s.recv(1)

        #If empty, the connection has been closed
        if not received:
            print("[-] ERROR: Connection with %s closed before get all headers." % (url.netloc))
            exit()

        headers += received

    # get HTTP response length
    headers_length = int(re.search(b"content-length:\s*(\d+)\s", headers, re.S + re.I).group(1))

    # read HTTP response body
    ocsp_resp = b''
    for i in range(headers_length):
        ocsp_resp += s.recv(1)

    return ocsp_resp

def get_ocsp_url(cert):
    # get OCSP url from certificate's AIA extension

    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
      componentType = namedtype.NamedTypes(
        namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
        namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
      componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:
        if str(seq[0])=='1.3.6.1.5.5.7.1.1': # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0]:
                if str(aia[0])=='1.3.6.1.5.5.7.48.1': # ocsp url
                    return str(aia[1].getComponentByName('uniformResourceIdentifier'))

    print("[-] OCSP url not found in certificate!")
    exit(1)

def get_issuer_cert_url(cert):
    # get CA certificate url from certificate's AIA extension (hint: see get_ocsp_url())
    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
      componentType = namedtype.NamedTypes(
        namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
        namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
      componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:
        if str(seq[0])=='1.3.6.1.5.5.7.1.1': # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0]:
                if str(aia[0])=='1.3.6.1.5.5.7.48.2': # ocsp url
                    return str(aia[1].getComponentByName('uniformResourceIdentifier'))

    print("[-] OCSP url not found in certificate!")
    exit(1)
    
def download_issuer_cert(issuer_cert_url):
    print("[+] Downloading issuer certificate from:", issuer_cert_url)

    # parse ocsp responder url
    url = urlparse(issuer_cert_url)
    
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((url.netloc, 80))

    # sent HTTP GET request
    request = "GET " + url.path + " HTTP/1.1\r\nHost: " + url.netloc + "\r\n\r\n"
    s.send(request.encode())

    # read HTTP response header
    headers = b''
    while b"\r\n\r\n" not in headers:
        received = s.recv(1)

        #If empty, the connection has been closed
        if not received:
            print("[-] ERROR: Connection with %s closed before get all headers." % (url.netloc))
            exit()

        headers += received

    # get HTTP response length
    headers_length = int(re.search(b"content-length:\s*(\d+)\s", headers, re.S + re.I).group(1))

    # read HTTP response body
    issuer_cert = b''
    for i in range(headers_length):
        issuer_cert += s.recv(1)

    return issuer_cert

# parses OCSP response
def parse_ocsp_resp(ocsp_resp):
    ocspResponse, _ = decoder.decode(ocsp_resp, asn1Spec=rfc2560.OCSPResponse())
    responseStatus = ocspResponse.getComponentByName('responseStatus')
    assert responseStatus == rfc2560.OCSPResponseStatus('successful'), responseStatus.prettyPrint()
    responseBytes = ocspResponse.getComponentByName('responseBytes')
    responseType = responseBytes.getComponentByName('responseType')
    assert responseType == rfc2560.id_pkix_ocsp_basic, responseType.prettyPrint()

    response = responseBytes.getComponentByName('response')

    basicOCSPResponse, _ = decoder.decode(
        response, asn1Spec=rfc2560.BasicOCSPResponse()
    )

    tbsResponseData = basicOCSPResponse.getComponentByName('tbsResponseData')

    response0 = tbsResponseData.getComponentByName('responses').getComponentByPosition(0)

    producedAt = datetime.datetime.strptime(str(tbsResponseData.getComponentByName('producedAt')), '%Y%m%d%H%M%SZ')
    certID = response0.getComponentByName('certID')
    certStatus = response0.getComponentByName('certStatus').getName()
    thisUpdate = datetime.datetime.strptime(str(response0.getComponentByName('thisUpdate')), '%Y%m%d%H%M%SZ')

    # let's assume that certID in response matches the certID sent in the request

    # let's assume that response signed by trusted responder

    print("[+] OCSP producedAt:", producedAt)
    print("[+] OCSP thisUpdate:", thisUpdate)
    print("[+] OCSP status:", certStatus)

cert = pem_to_der(open(sys.argv[1], 'rb').read())

ocsp_url = get_ocsp_url(cert)
print("[+] URL of OCSP responder:", ocsp_url)

issuer_cert_url = get_issuer_cert_url(cert)
issuer_cert = download_issuer_cert(issuer_cert_url)

ocsp_req = produce_request(cert, issuer_cert)
ocsp_resp = send_req(ocsp_req, ocsp_url)
parse_ocsp_resp(ocsp_resp)
