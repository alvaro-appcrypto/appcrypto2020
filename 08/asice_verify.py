#!/usr/bin/python3

# do not use any other imports/libraries
import codecs
import hashlib
import datetime
import sys
import zipfile

# apt-get install python3-bs4 python3-pyasn1-modules python3-ecdsa python3-m2crypto python3-lxml
from ecdsa import VerifyingKey
from bs4 import BeautifulSoup
from pyasn1.codec.der import decoder, encoder
from pyasn1_modules import rfc2560

# took 7.0 hours (please specify here how much time your solution required)

# verifies signature using ECDSA with SHA384
def verify_ecdsa(cert, signature_value, signed_msg):
    import M2Crypto
    X509 = M2Crypto.X509.load_cert_der_string(cert)
    pk = X509.get_pubkey()
    pubkey = pk.as_der() # public key info structure

    # ecdsa-sha384:
    vk = VerifyingKey.from_der(pubkey)
    return vk.verify(signature_value, signed_msg, hashfunc=hashlib.sha384)

# extracts from TSA response the timestamp and timestamped DigestInfo
def parseTsaResponse(timestamp_resp):
    timestamp = decoder.decode(timestamp_resp)
    tsinfo = decoder.decode(timestamp[0][1][2][1])[0]
    ts_digestinfo = encoder.encode(tsinfo[2])
    ts = datetime.datetime.strptime(str(tsinfo[4]), '%Y%m%d%H%M%SZ')
    # let's assume that timestamp has been issued by a trusted TSA
    return ts, ts_digestinfo

# extracts from OCSP response certID_serial, certStatus and thisUpdate
def parseOcspResponse(ocsp_resp):
    ocspResponse, _ = decoder.decode(ocsp_resp, asn1Spec=rfc2560.OCSPResponse())
    responseStatus = ocspResponse.getComponentByName('responseStatus')
    assert responseStatus == rfc2560.OCSPResponseStatus('successful'), responseStatus.prettyPrint()
    responseBytes = ocspResponse.getComponentByName('responseBytes')
    responseType = responseBytes.getComponentByName('responseType')
    assert responseType == rfc2560.id_pkix_ocsp_basic, responseType.prettyPrint()
    response = responseBytes.getComponentByName('response')
    basicOCSPResponse, _ = decoder.decode(response, asn1Spec=rfc2560.BasicOCSPResponse())
    tbsResponseData = basicOCSPResponse.getComponentByName('tbsResponseData')
    response0 = tbsResponseData.getComponentByName('responses').getComponentByPosition(0)
    # let's assume that OCSP response has been signed by a trusted OCSP responder
    certID = response0.getComponentByName('certID')
    # let's assume that issuer name and key hashes in certID are correct
    certID_serial = certID[3]
    certStatus = response0.getComponentByName('certStatus').getName()
    thisUpdate = datetime.datetime.strptime(str(response0.getComponentByName('thisUpdate')), '%Y%m%d%H%M%SZ')

    return certID_serial, certStatus, thisUpdate


# returns XML canonicalization of element with specified tagname
def canonicalize(full_xml, tagname):
    if type(full_xml)!=bytes:
        print("[-] canonicalize(): input not xml:", type(full_xml))
        exit(1)
    import io
    import lxml.etree as ET
    input = io.BytesIO(full_xml)
    et = ET.parse(input)
    output = io.BytesIO()
    ET.ElementTree(et.find('.//{*}'+tagname)).write_c14n(output)
    return output.getvalue()

# returns CommonName value from certificate's Subject Distinguished Name field
def get_subject_cn(cert_der):
    # looping over Distinguished Name entries until CN found
    distinguished_names = decoder.decode(cert_der)[0][0][5]
    for entry in distinguished_names:
        if list(entry[0][0]) == [2, 5, 4, 3]:
            return entry[0][1]
    return ''

##### My extra functions

def calc_sha256(content):
    digest = hashlib.sha256()
    i = 0
    while True:
        chunk = content[i*512:(i+1)*512]
        i += 1
        if not chunk:
            break
        digest.update(chunk)

    return digest.digest()


def get_expiration_timestamp(cert_der):
    timestamp = decoder.decode(cert_der)[0][0][4][1]
    date = datetime.datetime.strptime(str(timestamp), '%y%m%d%H%M%SZ')
    return date


def get_serial(cert_der):
    serial = decoder.decode(cert_der)[0][0][1]
    return serial

#######

# get and decode xml
filename = sys.argv[1]

archive = zipfile.ZipFile(filename, 'r')
xml = archive.read('META-INF/signatures0.xml')
xmldoc = BeautifulSoup(xml, features="xml")


# let's trust this certificate
signers_cert_der = codecs.decode(xmldoc.XAdESSignatures.KeyInfo.X509Data.X509Certificate.encode_contents(), 'base64')
print("[+] Signatory:", get_subject_cn(signers_cert_der))


#### Perform all kinds of checks
signed_file = xmldoc.XAdESSignatures.Signature.SignedInfo.Reference['URI']
print("[+] Signed file:", signed_file)

# Verify the digital signature of the file
file_content = archive.read(signed_file)
file_signature = calc_sha256(file_content)

xml_file_signature = codecs.decode(xmldoc.XAdESSignatures.Signature.SignedInfo.find('Reference', attrs={'Id':'S0-RefId0'}).DigestValue.encode_contents(), 'base64')

if xml_file_signature != file_signature:
    print("[-] The hash of the file " + str(signed_file) + " does not match the digital signature")
    sys.exit()

# Check whether the signature of the X509 cert from the signed properties 
x509_signature = calc_sha256(signers_cert_der)

x509_properties_signature = codecs.decode(xmldoc.XAdESSignatures.Signature.SignedProperties.SigningCertificate.DigestValue.encode_contents(), 'base64')

if x509_signature != x509_properties_signature:
    print("[-] The signature of the certificate is not valid")
    sys.exit()


# Verify signed properties
properties_str = canonicalize(xml, "SignedProperties")
properties_signature = calc_sha256(properties_str)

xml_properties_signature = codecs.decode(xmldoc.XAdESSignatures.Signature.SignedInfo.find('Reference', attrs={'URI':'#S0-SignedProperties'}).DigestValue.encode_contents(), 'base64')

if xml_properties_signature != properties_signature:
    print("[-] The hash of the signed properties does not match with the digital signature")
    sys.exit()


# Checks whether the sigature from the TSA response matches the digital signature
timestamp_resp = codecs.decode(xmldoc.XAdESSignatures.Signature.SignatureTimeStamp.EncapsulatedTimeStamp.encode_contents(), 'base64')
ts, ts_info = parseTsaResponse(timestamp_resp)
tsa_signature = decoder.decode(ts_info)[0][1].asOctets()

canon_signature = canonicalize(xml, "SignatureValue")
signature_sha2 = calc_sha256(canon_signature)

if tsa_signature != signature_sha2:
    print("[-] The data from the TSA response does not match the digital signature")
    sys.exit()

print("[+] Timestamped: ", ts)

# Check OSCP response
ocsp_resp = codecs.decode(xmldoc.XAdESSignatures.Signature.OCSPValues.EncapsulatedOCSPValue.encode_contents(), 'base64')

certID_serial, certStatus, thisUpdate = parseOcspResponse(ocsp_resp)

# check if revoked
if certStatus != "good":
    print("[-] The signatory's certificate is not valid ")
    sys.exit()

#Check if serial matches
serial = get_serial(signers_cert_der)
if certID_serial != serial:
    print("[-] The serial in the OCSP response does not match the certificate's serial")
    sys.exit()

# check if timestamp OCSP > TSA
if thisUpdate < ts:
    print("[-] The OCSP response was not performed after the TSA response")
    sys.exit()

# Canonical signed info
signed_info_str = canonicalize(xml, "SignedInfo")
# Get signature value
signature_value = codecs.decode(xmldoc.XAdESSignatures.Signature.SignatureValue.encode_contents() , 'base64')

# finally verify signatory's signature
try:
    if verify_ecdsa(signers_cert_der, signature_value, signed_info_str):
        print("[+] Signature verification successful!")
except:
    print("The digital signature is invalid")