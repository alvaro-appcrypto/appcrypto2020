package appcrypto;
import javacard.framework.*;
import javacard.security.*;
import javacardx.crypto.*;

//took 10 hours (please specify here how much time your solution required)

public class TestApplet extends Applet {
	RandomData rnd;
	KeyPair keypair;
	RSAPublicKey pub;
	Cipher rsa;
	
	public static void install(byte[] ba, short ofs, byte len) {
		(new TestApplet()).register();
	}
	public void process(APDU apdu) {
		byte[] buf = apdu.getBuffer(); // contains first 5 APDU bytes
		short len;
		
		switch (buf[ISO7816.OFFSET_INS]) {
		
			case (byte)0x00:
				if (buf[ISO7816.OFFSET_LC] != (byte)1) {
					ISOException.throwIt(ISO7816.SW_DATA_INVALID);
				}
				apdu.setIncomingAndReceive(); // read APDU data bytes
				len = (short)(buf[ISO7816.OFFSET_CDATA] & (short)0xff); // get rid of sign
				rnd = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM);
				rnd.generateData(buf, (short)0, len);
				apdu.setOutgoingAndSend((short)0, len); // return response data
				return;
			
			case (byte)0x02:
				if (keypair != null) {
					return;
				}
				keypair = new KeyPair(KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_2048);
				keypair.genKeyPair();
				return;
			
			case (byte)0x04:
				byte[] exponent = new byte[64];
				pub = (RSAPublicKey) keypair.getPublic();
				len = (short) (pub.getExponent(exponent, (short) 0) & (short) 0xff);
				Util.arrayCopyNonAtomic(exponent,(short)0,buf, (short) 0 ,len);
				apdu.setOutgoingAndSend((short) 0, len);
				return;
					
			case (byte)0x06:
				byte[] modulus = new byte[256];
				pub = (RSAPublicKey) keypair.getPublic();
				len = pub.getModulus(modulus, (short) 0);
				Util.arrayCopyNonAtomic(modulus,(short)0,buf,(short) 0, len );
				apdu.setOutgoingAndSend((short) 0, len);
				return;
				
			case (byte)0x08:
				byte[] ciphertext = new byte[256];
				ciphertext[0] = buf[ISO7816.OFFSET_P1];
				ciphertext[1] = buf[ISO7816.OFFSET_P2];
				len = (short)(buf[ISO7816.OFFSET_LC] & (short)0xff);
				apdu.setIncomingAndReceive(); // read APDU data bytes
				Util.arrayCopyNonAtomic(buf,ISO7816.OFFSET_CDATA, ciphertext,(short) 2, len);
				rsa = Cipher.getInstance(Cipher.ALG_RSA_PKCS1, false);
				rsa.init(keypair.getPrivate(), Cipher.MODE_DECRYPT);
				len = rsa.doFinal(ciphertext, (short) 0, (short) (len+2), buf, (short) 0);
				apdu.setOutgoingAndSend((short) 0, len);
				return;
			
		}
			
		ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
	}
	
}


