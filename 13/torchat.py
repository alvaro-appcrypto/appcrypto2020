#!/usr/bin/env python3

# sudo apt install python3-socks
import argparse
import socks
import socket
import sys
import random
# do not use any other imports/libraries

# took 4.5 hours (please specify here how much time your solution required)

# parse arguments
parser = argparse.ArgumentParser(description='TorChat client')
parser.add_argument('--myself', required=True, type=str, help='My TorChat ID')
parser.add_argument('--peer', required=True, type=str, help='Peer\'s TorChat ID')
args = parser.parse_args()

# route outgoing connections through Tor
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket

# reads and returns torchat command from the socket
def read_torchat_cmd(incoming_socket):
    # read until newline
    cmd = ""
    while True:
        cmd += incoming_socket.recv(1).decode("utf-8")
        if cmd[-1] == '\n':
            break

    # return command
    return cmd[:-1]

# prints torchat command and sends it
def send_torchat_cmd(outgoing_socket, cmd):

    print("[+] Sending:", cmd)
    cmd += '\n'
    outgoing_socket.send(cmd.encode())
    return



outgoing_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
incoming_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
incoming_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
incoming_socket.bind(('', 8888))

# connecting to peer
print("[+] Connecting to peer", args.peer)
outgoing_socket.connect((args.peer + ".onion", 11009))


# sending ping
cookie = '%0d' % random.getrandbits(32 * 8)
cmd = 'ping ' + args.myself + ' ' + str(cookie) + '\n'

print("[+] Sending:", cmd.replace('\n', ''))

outgoing_socket.send(cmd.encode())


# listening for the incoming connection
incoming_socket.listen(0)
print("[+] Listening...")

(s_torchat, address) = incoming_socket.accept()

print("[+] Client %s:%s" % (address[0], address[1]))


incoming_authenticated = False
status_received = False
cookie_peer = ""

# main loop for processing the received commands
while True:
    try:
        cmd_in = read_torchat_cmd(s_torchat)
        print("[+] Received: " + cmd_in) 

        cmd = cmd_in.split(' ')
        if cmd[0] == 'ping':
            if cmd[1] == args.peer:
                cookie_peer = cmd[2]
                if incoming_authenticated:
                    cmd_out = 'pong ' + cookie_peer
                    send_torchat_cmd(outgoing_socket, cmd_out)

        elif cmd[0] == 'pong':
            if cmd[1] == cookie:
                print("[+] Incoming connection authenticated!")
                incoming_authenticated = True
                cmd_out = 'pong ' + cookie_peer
                send_torchat_cmd(outgoing_socket, cmd_out)

            else:
                print('[-] ERROR: Incorrect pong')
                s_torchat.close()
                incoming_socket.close()
                outgoing_socket.close()
                sys.exit()

        else:
            if incoming_authenticated:
                if cmd[0] == 'status' and cmd[1] == 'available':
                    if not status_received:
                        send_torchat_cmd(outgoing_socket, 'profile_name Batman')
                        send_torchat_cmd(outgoing_socket, 'add_me')
                        send_torchat_cmd(outgoing_socket, 'status available')
                        status_received = True

                elif cmd[0] == 'message':
                    message = input('[?] Enter message: ')
                    send_torchat_cmd(outgoing_socket, 'message ' + message)

                else:
                    pass

            else:
                print('[-] ERROR: Connection not authenticated')
                s_torchat.close()
                incoming_socket.close()
                outgoing_socket.close()
                sys.exit()

    except KeyboardInterrupt:
        print('\n[+] Disconnecting')
        s_torchat.close()
        incoming_socket.close()
        outgoing_socket.close()
        sys.exit()

