#!/usr/bin/env python3

import argparse, hashlib, sys, datetime # do not use any other imports/libraries

# took 2.0 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='Proof-of-work solver')
parser.add_argument('--difficulty', default=0, type=int, help='Number of leading zero bits')
args = parser.parse_args()



def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b

def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i

def proof_of_work(prefix, difficulty_bits):
	max_nonce = 2 ** (difficulty_bits + 1)
	target = 2 ** (256-difficulty_bits)
	for nonce in range(max_nonce):
		hash_input = prefix.encode() + nb(nonce, 8)

		hash_result = hashlib.sha256(hashlib.sha256(hash_input).digest()).hexdigest()

		if int(hash_result, 16) <= target:
			return (hash_input.hex(), hash_result, nonce)

	print("[-] Failed after %d tries" % nonce)
	sys.exit()


identity = "Alvaro S"
hash_result = ''

start = datetime.datetime.now()

(hash_input, hash_result, nonce) = proof_of_work(identity, args.difficulty)

end = datetime.datetime.now()
elapsed = (end - start).total_seconds()

hash_sec = nonce/(elapsed*1000000)


print("[+] Solved in: " + str(elapsed) + " sec (" + str(hash_sec) + " Mhash/sec)")
print("[+] Input: " + hash_input)
print("[+] Solution: " + hash_result)
print("[+] Nonce: " + str(nonce))
