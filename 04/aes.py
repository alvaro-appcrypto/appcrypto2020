#!/usr/bin/python3

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python3-crypto
sys.path = sys.path[1:] # removes script directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
from hashlib import pbkdf2_hmac
import hashlib, hmac # do not use any other imports/libraries

# took 6.0 hours (please specify here how much time your solution required)

CHUNK_SIZE = 512
BLOCK_SIZE = 16
BENCHMARK_ITERATIONS = 10000

# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():

    # measure time for performing 10000 iterations
    start = datetime.datetime.now()
    pbkdf2_hmac('sha1', b'test', os.urandom(8), iterations=BENCHMARK_ITERATIONS,  dklen=(BLOCK_SIZE + 20))
    stop = datetime.datetime.now()
    time = (stop - start).total_seconds()

    # extrapolate to 1 second
    iter = int(BENCHMARK_ITERATIONS / time)

    print("[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter))

    return iter # returns number of iterations that can be performed in 1 second

def encrypt(pfile, cfile):

    # benchmarking
    iter = benchmark()

    # asking for password
    password = input("[?] Enter key: ").encode()

    # derieving key
    salt = os.urandom(8)
    key = pbkdf2_hmac('sha1', password, salt, iterations=iter, dklen=(BLOCK_SIZE + 20))
    key_AES = key[:BLOCK_SIZE]
    key_HMAC = key[BLOCK_SIZE:]

    # writing ciphertext in temporary file and calculating HMAC digest
    iv = iv_current = os.urandom(BLOCK_SIZE)
    hmac_ciphertext = hmac.new(key_HMAC, None, hashlib.sha1)
    cipher = AES.new(key_AES)

    with open(pfile, 'rb') as plain_file:
        with open(cfile + '.temp', 'ab') as temp_file:
            cipher_block = ''
            plaintext_block = ''
            while True:
                plaintext_chunk = plain_file.read(CHUNK_SIZE)
                if not plaintext_chunk:
                    # Padding PKCS#5
                    if len(plaintext_block) % BLOCK_SIZE != 0:
                        padding_length = BLOCK_SIZE - len(plaintext_block) % BLOCK_SIZE
                        plaintext_block = plaintext_block + bytes([padding_length]) * padding_length
                    else:
                        plaintext_block = bytes([BLOCK_SIZE]) * BLOCK_SIZE

                    cipher_block = cipher.encrypt(strxor(plaintext_block, iv_current))
                    hmac_ciphertext.update(cipher_block)

                    temp_file.write(cipher_block)
                    break

                # cipher the chunk by blocks
                for i in range(0, len(plaintext_chunk), BLOCK_SIZE):
                    plaintext_block = plaintext_chunk[i:i + BLOCK_SIZE]

                    # When the sizes are not the same, go to padding
                    if len(plaintext_block) % BLOCK_SIZE != 0:
                        break

                    cipher_block = cipher.encrypt(strxor(plaintext_block, iv_current))
                    hmac_ciphertext.update(cipher_block)

                    temp_file.write(cipher_block)

                    iv_current = cipher_block

    # writing DER structure in cfile
    with open(cfile, 'wb') as cipher_file:
        der = asn1_sequence(
            asn1_sequence(
                asn1_octetstring(salt) +
                asn1_integer(iter) +
                asn1_integer(BLOCK_SIZE + 20)) +
            asn1_sequence(
                asn1_objectidentifier([2, 16, 840, 1, 101, 2, 4, 1, 2]) +
                asn1_octetstring(iv)) +
            asn1_sequence(
                asn1_sequence(
                    asn1_objectidentifier([1, 3, 14, 3, 2, 26]) +
                    asn1_null()) +
                asn1_octetstring(hmac_ciphertext.digest())))

        cipher_file.write(der)

    # writing temporary ciphertext file to cfile
    with open(cfile, 'ab') as cipher_file:
        with open(cfile + '.temp', 'rb') as temp_file:
            cipher_file.write(temp_file.read())

    # deleting temporary ciphertext file
    os.unlink(cfile + '.temp')


def decrypt(cfile, pfile):

    extra_length = 0
    aux_length = 0
    # reading DER structure
    with open(cfile, 'rb') as cipher_file:
        # get DER length
        der_length = cipher_file.read(2)[1]
        # When the size byte is bigger than 128
        if der_length > 128:
            aux_length = der_length & 0x7F
            der_length = bn(cipher_file.read(aux_length))

        extra_length = 2 + aux_length

        # seek to the begining
        cipher_file.seek(0)
        # get der and ciphertext
        der, ciphertext = decoder.decode(cipher_file.read())

    salt = bytes(der[0][0])
    iter = int(der[0][1])
    key_length = int(der[0][2])
    iv = bytes(der[1][1])
    hmac_ciphertext = bytes(der[2][1])

    # asking for password
    password = input("[?] Enter key: ").encode()

    # derieving key
    key = pbkdf2_hmac('sha1', password, salt, dklen=key_length, iterations=iter)
    key_AES = key[:BLOCK_SIZE]
    key_HMAC = key[BLOCK_SIZE:]

    # first pass over ciphertext to calculate and verify HMAC
    hmac_calc = hmac.new(key_HMAC, None, hashlib.sha1)
    with open(cfile, 'rb') as cipher_file:
        #Extra length = 2 bytes to read the legth + possible bytes if length > 128
        cipher_file.seek(extra_length + der_length)
        while True:
            chunk = cipher_file.read(CHUNK_SIZE)
            if not chunk:
                break
            hmac_calc.update(chunk)

    if hmac_calc.digest() != hmac_ciphertext:
        print("[-] Wrong key or message has been manipulated!")
        exit()
    else:
        print("[+] HMAC verification successful!")

    # second pass over ciphertext to decrypt
    hmac_plaintext = hmac.new(key_HMAC, None, hashlib.sha1)
    cipher = AES.new(key_AES)

    with open(cfile, 'rb') as cipher_file:
        #Extra length = 2 bytes to read the legth + possible bytes if length > 128
        cipher_file.seek(extra_length + der_length)
        with open(pfile, 'ab') as plain_file:
            plain_block = ''
            while True:
                cipher_chunk = cipher_file.read(CHUNK_SIZE)
                if not cipher_chunk:
                    # remove padding
                    plain_file.seek(-plain_block[-1], os.SEEK_END)
                    plain_file.truncate()
                    break

                for i in range(0, len(cipher_chunk), BLOCK_SIZE):
                    plain_block = strxor(iv, cipher.decrypt(cipher_chunk[i:i + BLOCK_SIZE]))
                    iv = cipher_chunk[i:i + BLOCK_SIZE]

                    plain_file.write(plain_block)
                    hmac_plaintext.update(plain_block)


def bn(b):
    # b - bytes to encode as integer
    i = 0
    for char in b:
        i = i << 8
        i = i | char
    return i

####### ASN1 definitions

def asn1_len(value_bytes):
    length = len(value_bytes)
    if length < 128:
        return bytes([length])
    else:
        b = b''
        following_bytes = 0
        while length:
            b = bytes([(length & 0xFF)]) + b
            length = length >> 8
            following_bytes = following_bytes + 1
        first_byte = bytes([(128 | following_bytes)])
        return first_byte + b

def asn1_octetstring(octets):
    return bytes([0x04]) + asn1_len(octets) + octets

def asn1_integer(i):
    integer = b''
    if i == 0:
        integer = bytes([0x00])
    else:
        while i:
            last = bytes([i & 0xFF])
            integer = last + integer
            i = i >> 8

        if last >= b'\x80':
            integer = b'\x00' + integer

    return bytes([0x02]) + asn1_len(integer) + integer

def asn1_objectidentifier(oid):
    first_byte = bytes([(40 * oid[0] + oid[1])])

    oid_bytes = []
    for i in oid[2:]:
        int_bytes = bytes([i & 0x7F])
        i = i >> 7
        while i:
            int_bytes = bytes([i & 0x7F | 0x80]) + int_bytes
            i = i >> 7

        oid_bytes.append(int_bytes)

    final_bytes = b''
    for j in oid_bytes:
        final_bytes = final_bytes + j 

    return bytes([0x06]) + asn1_len(first_byte+final_bytes) + first_byte + final_bytes

def asn1_sequence(der):
    return bytes([0x30]) + asn1_len(der) + der

def asn1_null():
    return bytes([0x05]) + bytes([0x00])

####### End ASN1 definition

def usage():
    print("Usage:")
    print("-encrypt <plaintextfile> <ciphertextfile>")
    print("-decrypt <ciphertextfile> <plaintextfile>")
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
